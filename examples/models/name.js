/**
 * @module banded/models/name
 */

'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NameSchema = new Schema({
  first: String,
  last: String
});

module.exports = mongoose.model('Name', NameSchema);
