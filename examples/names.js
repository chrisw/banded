'use strict';

var fs = require('fs');
var path = require('path');

var mongoose = require('mongoose');
var Name = require('./models/name');

var band = require('..');

mongoose.connect('mongodb://localhost/banded');
var conn = mongoose.connection;

var options = {
  file: path.join(__dirname, 'names.csv'),
  model: Name
};

band(options, displayDB);

function displayDB() {
  Name.find({}, onFind);

  function onFind(err, names) {
    console.log(names);

    Name.remove({}, conn.close.bind(conn));
  }
}
