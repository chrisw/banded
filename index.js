/**
 * @module banded/index
 */

'use strict';

var assert = require('assert');
var fs = require('fs');

var ParserStream = require('./lib/parserStream');
var PreTransformStream = require('./lib/preTransformStream');
var ModelStream = require('./lib/modelStream');
var PostTransformStream = require('./lib/postTransformStream');
var StorageStream = require('./lib/storageStream');

function band(options, callback) {
  assert(options.file, 'File must be supplied');

  var readStream = fs.createReadStream(options.file);

  var parserStream = new ParserStream(options);
  var preTransformStream = new PreTransformStream(options);
  var modelStream = new ModelStream(options);
  var postTransformStream = new PostTransformStream(options);
  var storageStream = new StorageStream();

  readStream
    .pipe(parserStream)
    .pipe(preTransformStream)
    .pipe(modelStream)
    .pipe(storageStream)
    .on('finish', callback);
}

module.exports = band;
