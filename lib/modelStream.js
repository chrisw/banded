/**
 * @module banded/modelStream
 */

'use strict';

var assert = require('assert');
var stream = require('readable-stream');
var merge = require('merge');
var util = require('util');

ModelStream.DEFAULTS = {
  columns: false,
  types: false
};

/**
 * Creates an instance of ModelStream
 * @constructor
 * @param {object} options
 */
function ModelStream(options) {
  var self = this;

  if(!(self instanceof ModelStream)) {
    return new ModelStream(options);
  }

  assert(options.model, 'Model must be supplied');

  self.options = merge(Object.create(ModelStream.DEFAULTS), options);

  stream.Transform.call(self, {
    objectMode: true
  });
}

util.inherits(ModelStream, stream.Transform);

/**
 * Creates a model from provided data
 * #_transform
 * @param {object} data
 * @param {string} encoding
 * @param {function} callback
 */
ModelStream.prototype._transform = function(data, encoding, callback) {
  var self = this;

  // assume the first line is for column names
  if(!Array.isArray(self.options.columns)) {
    if(typeof self.options.columns === 'function') {
      data = data.map(self.options.columns);
    }

    self.options.columns = data;

    return callback();
  }

  // build an object of key (columns) value (data) pairs
  var modelProperties = data.reduce(into, {});

  callback(null, new self.options.model(modelProperties));

  function into(object, value, index) {
    var key = self.options.columns[index];

    if(self.options.types) {
      value = self.options.types[index](value);
    }

    object[key] = value;

    return object;
  }
};

module.exports = ModelStream;
