/**
 * @module banded/parserStream
 */

'use strict';

var stream = require('readable-stream');
var merge = require('merge');
var util = require('util');

ParserStream.DEFAULTS = {
  delimiter: ',',
  rowDelimiter: '\n'
};

/**
 * Creates an instance of ParserStream
 * @constructor
 * @param {object} options
 */
function ParserStream(options) {
  var self = this;

  if(!(self instanceof ParserStream)) {
    return new ParserStream(options);
  }

  self.options = merge(Object.create(ParserStream.DEFAULTS), options || {});
  self.buffer = '';

  stream.Transform.call(self, {
    objectMode: true
  });
}

util.inherits(ParserStream, stream.Transform);

/**
 * Buffers data into rows
 * #_transform
 * @param {object} data
 * @param {string} encoding
 * @param {function} callback
 */
ParserStream.prototype._transform = function(data, encoding, callback) {
  var self = this;

  var str = data.toString();
  var lines = str.split(self.options.rowDelimiter);

  // lines with ends will be processed
  while(lines.length > 1) {
    self._processLine(lines.shift());
  }

  // the reminder is stored in the buffer to be processed
  // with the next batch
  self.buffer += lines.shift();

  callback();
};

/**
 * Flush buffer and line
 * #_flush
 * @param {function} callback
 */
ParserStream.prototype._flush = function(callback) {
  var self = this;

  // the passed in line will be combined with the buffer
  // we pass in the empty string so that only the buffer is used
  self._processLine('');

  callback();
};

/**
 * Process and push a line of data
 * #_processLines
 * @param {string} line
 */
ParserStream.prototype._processLine = function(line) {
  var self = this;

  var fullLine = self.buffer + line;
  var attributes = fullLine.split(self.options.delimiter);

  self.buffer = '';
  self.push(attributes);
};

module.exports = ParserStream;
