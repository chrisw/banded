/**
 * @module banded/postTransformStream
 */

'use strict';

var stream = require('readable-stream');
var merge = require('merge');
var util = require('util');

PostTransformStream.DEFAULTS = {
  postTransform: function() {}
};

/**
 * Creates an instance of PostTransformStream
 * @constructor
 * @param {object} options
 */
function PostTransformStream(options) {
  var self = this;

  if(!(self instanceof PostTransformStream)) {
    return new PostTransformStream(options);
  }

  self.options = merge(Object.create(PostTransformStream.DEFAULTS), options || {});

  stream.Transform.call(self, {
    objectMode: true
  });
}

util.inherits(PostTransformStream, stream.Transform);

/**
 * Transform raw data row by row
 * #_transform
 * @param {object} data
 * @param {string} encoding
 * @param {function} callback
 */
PostTransformStream.prototype._transform = function(data, encoding, callback) {
  var self = this;

  self.options.postTransform(data);

  callback(null, data);
};

module.exports = PostTransformStream;
