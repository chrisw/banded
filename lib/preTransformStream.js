/**
 * @module banded/preTransformStream
 */

'use strict';

var assert = require('assert');
var stream = require('readable-stream');
var merge = require('merge');
var util = require('util');

PreTransformStream.DEFAULTS = {
  preTransform: function(data) { return data; },
  aggregate: false,
};

/**
 * Creates an instance of PreTransformStream
 * @constructor
 * @param {object} options
 */
function PreTransformStream(options) {
  var self = this;

  if(!(self instanceof PreTransformStream)) {
    return new PreTransformStream(options);
  }

  self.aggregate = [];
  self.options = merge(Object.create(PreTransformStream.DEFAULTS), options || {});

  if(self.options.aggregate) {
    assert(options.aggregate.rows, 'Aggregate Rows must be supplied');
  }

  stream.Transform.call(self, {
    objectMode: true
  });
}

util.inherits(PreTransformStream, stream.Transform);

/**
 * Transform raw data row by row
 * #_transform
 * @param {object} data
 * @param {string} encoding
 * @param {function} callback
 */
PreTransformStream.prototype._transform = function(data, encoding, callback) {
  var self = this;

  // no need to transform the first row (columns)
  if(!self.passed) {
    self.passed = true;

    if(!Array.isArray(self.options.columns)) {
      return callback(null, data);
    }
  }

  if(self.options.aggregate) {
    self._aggregate(data);

    return callback();
  }

  data = self.options.preTransform(data);

  callback(null, data);
};

/**
 * Aggregate data
 * #_aggregate
 * @param {object} data
 */
PreTransformStream.prototype._aggregate = function(data) {
  var self = this;

  self.aggregate.push(data);

  if(self.aggregate.length === self.options.aggregate.rows) {
    data = self.options.preTransform(self.aggregate);

    if(self.options.aggregate.moving) {
      self.aggregate.shift();
    } else {
      self.aggregate = [];
    }

    self.push(data);
  }
};

module.exports = PreTransformStream;
