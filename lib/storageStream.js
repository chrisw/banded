/**
 * @module banded/storageStream
 */

'use strict';

var stream = require('readable-stream');
var util = require('util');

/**
 * Creates an instance of StorageStream
 * @constructor
 */
function StorageStream() {
  var self = this;

  if(!(self instanceof StorageStream)) {
    return new StorageStream();
  }

  stream.Writable.call(self, {
    objectMode: true
  });
}

util.inherits(StorageStream, stream.Writable);

/**
 * Saves models to the database
 * #_write
 * @param {object} data
 * @param {string} encoding
 * @param {function} callback
 */
StorageStream.prototype._write = function(data, encoding, callback) {
  data.save(callback);
};

module.exports = StorageStream;
