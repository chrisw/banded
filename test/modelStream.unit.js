'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var ModelStream = require('../lib/modelStream');
var stream = require('readable-stream');

var options = {
  model: Object
};

describe('ModelStream', function() {
  describe('@constructor', function() {
    it('should create an instance of PreTransformStream', function() {
      expect(new ModelStream(options)).to.be.instanceOf(ModelStream);
    });

    it('should create an instance of PreTransformStream without the `new` keyword', function() {
      expect(ModelStream(options)).to.be.instanceOf(ModelStream);
    });

    it('should create an instance of Transform', function() {
      expect(new ModelStream(options)).to.be.instanceOf(stream.Readable);
    });
  });

  describe('#_transform', function() {
    var readable;
    var testData;

    beforeEach(function() {
      var index = 0;
      testData = [
        ['column'],
        ['data'],
        null // read streams are null terminated
      ];

      readable = new stream.Readable({
        objectMode: true
      });

      readable._read = function() {
        this.push(testData[index++]);
      }
    });

    it('should set columns as first row if columns are not provided', function() {
      var modelStream = ModelStream(options);

      readable
        .pipe(modelStream)
        .on('data', onData);

      function onData(data) {
        expect(modelStream.options.columns).to.equal(testData[0]);
        expect(data.column).to.equal('data');
        done();
      }
    });

    it('should apply column function to first row if provided', function(done) {
      var modelStream = ModelStream(options);
      modelStream.options.columns = function(header) {
        return header.toUpperCase();
      };

      readable
        .pipe(modelStream)
        .on('data', onData);

      function onData(data) {
        expect(modelStream.options.columns[0]).to.equal('COLUMN');
        expect(data.COLUMN).to.equal('data');
        done();
      }
    });

    it('should use the columns provided by options', function(done) {
      testData.shift();

      var modelStream = ModelStream(options);
      modelStream.options.columns = ['header']

      readable
        .pipe(modelStream)
        .on('data', onData);

      function onData(data) {
        expect(modelStream.options.columns[0]).to.equal('header');
        expect(data.header).to.equal('data');
        done();
      }
    });

    it('should convert to the appropriate types if supplied', function(done) {
      testData[1][0] = '1';

      var modelStream = ModelStream(options);
      modelStream.options.types = [Number];

      readable
        .pipe(modelStream)
        .on('data', onData);

      function onData(data) {
        expect(data.column).to.equal(1);
        done();
      }
    });
  });
});
