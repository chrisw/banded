'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var ParserStream = require('../lib/parserStream');
var stream = require('readable-stream');

describe('ParserStream', function() {
  describe('@constructor', function() {
    it('should create an instance of ParserStream', function() {
      expect(new ParserStream()).to.be.instanceOf(ParserStream);
    });

    it('should create an instance of ParserStream without `new` keyword', function() {
      expect(ParserStream()).to.be.instanceOf(ParserStream);
    });

    it('should create an instance of Transform', function() {
      expect(new ParserStream()).to.be.instanceOf(stream.Transform);
    });
  })

  describe('#_processLine', function() {
    var p;

    before(function() {
      p = sinon.stub(ParserStream.prototype, 'push');
    });

    after(function() {
      p.restore();
    });

    afterEach(function() {
      p.reset();
    });

    it('should push a line of text split be delimiters', function() {
      var line = 'This,is,a,full,line';
      var processedLine = ['This', 'is', 'a', 'full', 'line']

      ParserStream()._processLine(line);

      expect(JSON.stringify(p.args[0][0])).to.equal(JSON.stringify(processedLine));
    });

    it('should complete the line with the buffer', function() {
      var buffer = 'This'
      var line = ',is,a,full,line';
      var processedLine = ['This', 'is', 'a', 'full', 'line']

      var parserStream = ParserStream();
      parserStream.buffer = buffer;

      parserStream._processLine(line);

      expect(JSON.stringify(p.args[0][0])).to.equal(JSON.stringify(processedLine));
    });
  });

  describe('#_flush', function() {
    var _pl;

    before(function() {
      _pl = sinon.stub(ParserStream.prototype, '_processLine');
    });

    after(function() {
      _pl.restore();
    });

    afterEach(function() {
      _pl.reset();
    });

    it('should process an empty line to clear the buffer', function(done) {
      ParserStream()._flush(callback);

      function callback() {
        expect(_pl).to.have.been.calledWith('');
        done();
      }
    });
  });

  describe('#_transform', function() {
    var readable;
    var testData;

    beforeEach(function() {
      var index = 0;

      readable = new stream.Readable({
        objectMode: true
      });

      readable._read = function() {

        // testData must be defined for each test
        this.push(testData[index++]);
      }
    });

    it('should process data into rows and cloumns', function(done) {
      testData = [
        'This,is,a,full,line\n',
        'This,is,almost,a',
        ',line\nAnd,then,the,',
        'extra,stuff',
        null // read streams are null terminated
      ];

      var processedData = [
        ['This', 'is', 'a', 'full', 'line'],
        ['This', 'is', 'almost', 'a', 'line'],
        ['And', 'then', 'the', 'extra', 'stuff']
      ];

      readable
        .pipe(ParserStream())
        .on('data', onData);

      var row = 0;

      function onData(data) {
        expect(JSON.stringify(data)).to.equal(JSON.stringify(processedData[row]));

        if(row++ >= processedData.length - 1) {
          done();
        }
      }
    });

    it('should use `delimiter` passed in from options', function(done) {
      testData = [
        'This;is;a;full;line\n',
        'This;is;almost;a',
        ';line\nAnd;then;the;',
        'extra;stuff',
        null // read streams are null terminated
      ];

      var processedData = [
        ['This', 'is', 'a', 'full', 'line'],
        ['This', 'is', 'almost', 'a', 'line'],
        ['And', 'then', 'the', 'extra', 'stuff']
      ];

      var options = {
        delimiter: ';'
      };

      readable
        .pipe(ParserStream(options))
        .on('data', onData);

      var row = 0;

      function onData(data) {
        expect(JSON.stringify(data)).to.equal(JSON.stringify(processedData[row]));

        if(row++ >= processedData.length - 1) {
          done();
        }
      }
    });

    it('should use `rowDelimiter` passed in from options', function(done) {
      testData = [
        'This,is,a,full,line\t',
        'This,is,almost,a',
        ',line\tAnd,then,the,',
        'extra,stuff',
        null // read streams are null terminated
      ];

      var processedData = [
        ['This', 'is', 'a', 'full', 'line'],
        ['This', 'is', 'almost', 'a', 'line'],
        ['And', 'then', 'the', 'extra', 'stuff']
      ];

      var options = {
        rowDelimiter: '\t'
      };

      readable
        .pipe(ParserStream(options))
        .on('data', onData);

      var row = 0;

      function onData(data) {
        expect(JSON.stringify(data)).to.equal(JSON.stringify(processedData[row]));

        if(row++ >= processedData.length - 1) {
          done();
        }
      }
    });
  });
});
