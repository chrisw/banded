'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var PostTransformStream = require('../lib/postTransformStream');
var stream = require('readable-stream');

describe('PostTransformStream', function() {
  describe('@constructor', function() {
    it('should create an instance of PostTrasformStream', function() {
      expect(new PostTransformStream()).to.be.instanceOf(PostTransformStream);
    });

    it('should create an istance of PostTransformStream without the `new` keyword', function() {
      expect(PostTransformStream()).to.be.instanceOf(PostTransformStream);
    });

    it('should create an instance of Transform', function() {
      expect(new PostTransformStream()).to.be.instanceOf(stream.Transform);
    });
  });

  describe('#_transform', function() {
    var readable;
    var testData;

    beforeEach(function() {
      var emitted = false;
      testData = {};

      readable = new stream.Readable({
        objectMode: true
      });

      readable._read = function() {
        if(!emitted) {
          this.push(testData);
        } else {

          // terminate stream
          this.push(null);
        }

        emitted = true;
      }
    });

    it('should modify data through `postTransform`', function() {
      readable
        .pipe(PostTransformStream())
        .once('data', onData);

      function onData(data) {
        expect(data).to.equal(testData);
      }
    });

    it('should modify data using custom `postTransform`', function() {
      var options = {
        postTransform: function(data) {
          data.property = 'value';
        }
      };

      readable
        .pipe(PostTransformStream(options))
        .once('data', onData);

      function onData(data) {
        expect(data.property).to.equal('value');
      }
    });
  });
});
