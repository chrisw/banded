'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var PreTransformStream = require('../lib/preTransformStream');
var stream = require('readable-stream');

describe('PreTransformStream', function() {
  describe('@constructor', function() {
    it('should create an instace of PreTransformStream', function() {
      expect(new PreTransformStream()).to.be.instanceOf(PreTransformStream);
    });

    it('should create an instance of PreTransformStream without the `new` keyword', function() {
      expect(PreTransformStream()).to.be.instanceOf(PreTransformStream);
    });

    it('should create an instance of Transform', function() {
      expect(new PreTransformStream()).to.be.instanceOf(stream.Transform);
    });

    it('should error if `rows` are not provided for aggregate option', function() {
      var options = {
        aggregate: true
      };

      expect(PreTransformStream.bind(undefined, options)).to.throw(Error);
    });
  });

  describe('#_aggregate', function() {
    it('should transform and push data at threshold', function(done) {
      var options = {
        aggregate: {
          rows: 1
        }
      };

      PreTransformStream(options)
        .on('data', onData)
        ._aggregate('data');

      function onData(data) {
        expect(data[0]).to.equal('data');
        done();
      }
    });

    it('should reset `aggregate` after push', function(done) {
      var options = {
        aggregate: {
          rows: 1
        }
      };

      var pts = PreTransformStream(options);

      pts
        .on('data', onData)
        ._aggregate('data');

      function onData(data) {
        expect(pts.aggregate).to.be.empty;
        done();
      }
    });

    it('should just shift aggregate if `moving`', function(done) {
      var options = {
        aggregate: {
          rows: 2,
          moving: true
        },
        preTransform: function(data) {
          expect(data).to.have.length(2);

          return data;
        }
      };

      var pts = PreTransformStream(options);

      pts.on('data', onData)
      pts._aggregate('data');
      pts._aggregate('data');

      function onData(data) {
        expect(pts.aggregate).to.have.length(1);
        done();
      }
    });
  });

  describe('#_transform', function() {
    var readable;
    var testData;

    beforeEach(function() {
      var index = 0;
      testData = [
        ['column'],
        ['data'],
        null // read streams are null terminated
      ];

      readable = new stream.Readable({
        objectMode: true
      });

      readable._read = function() {
        this.push(testData[index++]);
      }
    });

    it('should skip over the first row if columns are not provided', function(done) {
      var times = 0;

      var options = {
        preTransform: function(rows) {
          return [];
        }
      };

      readable
        .pipe(PreTransformStream(options))
        .on('data', onData);

      function onData(data) {
        if(!(times++)) {
          expect(data[0]).to.equal(testData[0][0]);
        } else {
          expect(data).to.be.empty;
          done();
        }
      }
    });

    it('should not skip over the first row if columns are provided', function() {
      testData.shift();

      var options = {
        columns: [],
        preTransform: function(rows) {
          return [];
        }
      };

      readable
        .pipe(PreTransformStream(options))
        .on('data', onData);

      function onData(data) {
        expect(data).to.be.empty;
        done();
      }
    });

    it('should transform through `_aggregate` if aggregating data', function() {
      testData.shift();
      var a = sinon.stub(PreTransformStream.prototype, '_aggregate', _aggregate);

      function _aggregate(data) {
        this.push(data);
      }

      var options = {
        columns: [],
        preTransform: function(rows) {
          return [];
        },
        aggregate: {
          rows: 1
        }
      };

      readable
        .pipe(PreTransformStream(options))
        .on('data', onData);

      function onData(data) {
        expect(a).to.have.been.called;
        a.restore();
        done();
      }
    });
  });
});
