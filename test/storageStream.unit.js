'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var StorageStream = require('../lib/storageStream');
var stream = require('readable-stream');

describe('StorageStream', function() {
  describe('@constructor', function() {
    it('should creaate an instance of StorageStream', function() {
      expect(new StorageStream()).to.be.instanceOf(StorageStream);
    });

    it('should create an instance of StorageStream without the `new` keyword', function() {
      expect(StorageStream()).to.be.instanceOf(StorageStream);
    });

    it('should create an instance of Writable', function() {
      expect(new StorageStream()).to.be.instanceOf(stream.Writable);
    });
  });

  describe('#_write', function() {
    var data = {};

    before(function() {
      data.save = sinon.stub();
    });

    afterEach(function() {
      data.save.reset();
    });

    it('should save the data passed in', function(done) {
      data.save.yields(null);
      StorageStream().write(data, 'encoding', cb);

      function cb(err) {
        expect(data.save).to.have.been.called;
        expect(err).to.not.be.ok;
        done();
      }
    });

    it('should emit an error if `save` fails', function(done) {
      data.save.yields(new Error('save failed'));

      var ss = StorageStream();

      ss.once('error', onError);
      ss.write(data);

      function onError(err) {
        expect(err.message).to.equal('save failed');
        done();
      }
    });
  });
});

